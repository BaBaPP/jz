# JZ
一个轻量、简单的弹窗插件（一个文件,含`css`,默认在`js`里面载入样式，少一次请求）

# 功能列表
 - msg  （提示消息，类似自带方法`alert`）
 - confirm  (询问提示，类似自带方法`confirm`)
 - iframe (弹出`iframe`)
 - tip  (气泡提示)

# 上手

引入`jz-1.0.0.min.js`
```javascript
<script src="jz-1.0.0.min.js"></script>
```

# 示例
Demo：[www.netnr.com/scripts/jz/1.0.0/demo.html](//www.netnr.com/scripts/jz/1.0.0/demo.html "具体使用示例")

# 主页
Domain：[www.netnr.com/jz](//www.netnr.com/jz "官方站点")
